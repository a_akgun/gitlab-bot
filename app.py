from llama_index import GPTSimpleVectorIndex, Document, SimpleDirectoryReader
from flask import Flask, request, render_template
import os

app = Flask(__name__)

@app.route('/')
def hello():
    q = request.args.get('q')
    if q:
        index = GPTSimpleVectorIndex.load_from_disk('database.json')
        rep = index.query("Answer for Gitlab database documentation: " + q)
        return render_template('index.html', q=q, result=rep)
    else:
      return render_template('index.html', q='', result='')
