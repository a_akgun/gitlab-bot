from llama_index import GPTSimpleVectorIndex, Document, SimpleDirectoryReader
import os

# Loading from a directory and save your index to a index.json file
documents = SimpleDirectoryReader('/home/aakgun/aakgun/1/gdk/gitlab/doc/development/database/').load_data()
index = GPTSimpleVectorIndex(documents)
index.save_to_disk('database.json')
